#![no_std]
#![no_main]

use arduino_hal::delay_ms;
use fixedstr::zstr;
use ssd1306::{prelude::*, I2CDisplayInterface, Ssd1306};
use panic_halt as _;

use core::fmt::Write;

fn part1_row(row: &str) -> u8 {
    let mut first = None;
    let mut last = None;
    for c in row.chars() {
        if c.is_ascii_digit() {
            if first.is_none() {
                first = c.to_digit(10);
            }
            last = c.to_digit(10);
        }
    }
    if let (Some(first), Some(last))= (first, last) {
        first as u8 * 10 + last as u8
    } else {
        0
    }
}

fn part2_row(row: &str) -> u8 {
    let mut first = None;
    let mut last = None;
    let mut found = |d| {
        if first.is_none() {
            first = d;
        }
        last = d;
    };
    for (i, c) in row.char_indices() {
        if c.is_ascii_digit() {
            found(c.to_digit(10));
        } else if row.get(i..i+4) == Some("zero") {
            found(Some(0));
        } else if row.get(i..i+3) == Some("one") {
            found(Some(1));
        } else if row.get(i..i+3) == Some("two") {
            found(Some(2));
        } else if row.get(i..i+5) == Some("three") {
            found(Some(3));
        } else if row.get(i..i+4) == Some("four") {
            found(Some(4));
        } else if row.get(i..i+4) == Some("five") {
            found(Some(5));
        } else if row.get(i..i+3) == Some("six") {
            found(Some(6));
        } else if row.get(i..i+5) == Some("seven") {
            found(Some(7));
        } else if row.get(i..i+5) == Some("eight") {
            found(Some(8));
        } else if row.get(i..i+4) == Some("nine") {
            found(Some(9));
        }
    }

    if let (Some(first), Some(last))= (first, last) {
        first as u8 * 10 + last as u8
    } else {
        0
    }
}

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    let mut serial =
        arduino_hal::Usart::new(dp.USART0, pins.d0, pins.d1.into_output(), 20000.into());
    let mut led = pins.d13.into_output();

    let i2c = arduino_hal::I2c::new(
        dp.TWI,
        pins.a4.into_pull_up_input(),
        pins.a5.into_pull_up_input(),
        50000,
    );

    let interface = I2CDisplayInterface::new(i2c);

    let mut display = Ssd1306::new(
        interface,
        DisplaySize128x32,
        DisplayRotation::Rotate0,
    ).into_terminal_mode();
    display.init().unwrap();
    display.clear().unwrap();

    let mut s: zstr<64> = zstr::new();
    let mut s = String::new();

    let mut sum1 = 0;
    let mut sum2 = 0;
    loop {
        let mut done = false;
        loop {
            serial.write_byte(1);
            let b = serial.read_byte();
            if b == b'\0' {
                done = true;
                break;
            }
            if b == b'\n' {
                break;
            }
            if let Ok(c) = b.try_into() {
                s.push_char(c);
            }
        }
        if done {
            break;
        }
        sum1 += part1_row(&s) as u32;
        sum2 += part2_row(&s) as u32;
        s.clear();
    }

    write!(display, "{}\n{}", sum1, sum2);

    loop {
        delay_ms(100);
    }
}
