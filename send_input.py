import time

import serial

with serial.Serial("/dev/ttyUSB0", 20000) as s:
    with open("input.txt") as f:
        lines = f.read().split("\n")
        for i, line in enumerate(lines):
            print("{}, {:.0f}%".format(i, 100 * i / len(lines)))
            for c in line:
                s.read(1)
                s.write(c.encode("ascii"))
            s.read(1)
            s.write(b"\n")
    s.read(1)
    s.write(b"\0")
    while True:
        time.sleep(1)
